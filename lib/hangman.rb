# require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  private

  def update_board(indices, letter)
    indices.each { |idx| @board[idx] = letter}
  end

  def over?
    board.count(nil) == 0
  end

  def conclude
    puts "Fin"
  end

end

class HumanPlayer
  def initialize
    @guessed_letters = []
  end

  def register_secret_length(length)
    print "The secret word has length #{length}"
  end

  def guess(board)
    print_board(board)
    print "> "
    guess = gets.chomp.downcase
    process_guess(guess)
  end

  def process_guess(guess)
    unless @guessed_letters.include?(guess)
        @guessed_letters << guess
        return guess
    else
      puts "Already guessed #{guess}"
      puts "Guessed letters: #{@guessed_letters}"
      puts ">"
      guess = gets.chomp.downcase
      process_guess(guess)
    end
  end

  def pick_secret_word
    print "Select the keyword's length"
    gets.chomp.to_i
  end

  def check_guess(letter)
      puts "Computer guess: #{letter}"
      print "Update correct indices"
      gets.chomp.split.map(&:to_i)
  end

  def handle_response(guess, indices)

  end

  private

  def print_board(board)
    board_string = board.map do |x|
      x.nil? ? "_" : x
    end.join("")
    puts "Secret word: #{board_string}"

  end

end

class ComputerPlayer
  def self.read_dictionary
    File.readlines('./lib/dictionary.txt').map(&:chomp)
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ("a".."z").to_a
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |chr, idx|
        indices << idx if chr == letter
    end
    indices
  end

  def register_secret_length(length)
   @candidate_words = @dictionary.select {|word| word.length == length }
 end


 def guess(board)
   opts = options(board)
   letter_count = Hash.new(0)
   opts.each do |char|
     letter_count[char] += 1
   end
   letter_count.sort_by { |char, occurance| occurance}[-1][0]


 end

 def options(board)
   letters = []
  @dictionary.each do |word|
     word.chars.each_with_index do |char, idx|
       letters << char if board[idx].nil?
     end
   end
   letters
 end


  def register_secret_length(length)
    @dictionary.select! {|word| word.length == length }
  end

  def candidate_words
    @dictionary
  end


  def handle_response(letter, indices)
      @dictionary.select! do |word|
        w_idx = []
        word.chars.each_with_index do |ch, idx|
          w_idx << idx if ch == letter
        end
        indices == w_idx
      end
  end

end


# if __FILE__ == $PROGRAM_NAME
#     dictionary = ComputerPlayer.read_dictionary
#   players = {
#     referee: HumanPlayer.new,
#     guesser: ComputerPlayer.new(dictionary)
#   }
#
#   game = Hangman.new(players)
#   game.play
# end
